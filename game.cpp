#include "game.h"
#include <cmath>
#include <ctime>
#include <iostream>

Game::Game() :
    BaseGame("~*~FLOAT~*~ Snake", sf::Vector2u(GAME_WINDOW_WIDTH, GAME_WINDOW_HEIGHT)),
    m_world(static_cast<sf::Vector2i>(m_window.getWindowSize())),
  m_snake(m_world.getBlockSize())
{
    srand(time(nullptr));
    m_textbox.setup(5, 14, 350, sf::Vector2f(GAME_WINDOW_WIDTH - 350,0));
    m_textbox.add("Seeded random number generator with " + std::to_string(time(nullptr)));
}

Game::~Game() { m_window.~Window(); }

void Game::handleInput() {
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up) && m_snake.getPhysicalDirection() != Direction::DOWN)
        m_snake.setDirection(Direction::UP);
    else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down) && m_snake.getPhysicalDirection() != Direction::UP)
        m_snake.setDirection(Direction::DOWN);
    else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left) && m_snake.getPhysicalDirection() != Direction::RIGHT)
        m_snake.setDirection(Direction::LEFT);
    else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right) && m_snake.getPhysicalDirection() != Direction::LEFT)
        m_snake.setDirection(Direction::RIGHT);
}

void Game::update()
{
    m_window.update();

    m_fElapsedAcc += getElapsedTime().asSeconds();

    float timeStep = 1.0f / m_snake.getSpeed();
    if (m_fElapsedAcc >= timeStep) {
        m_snake.tick();
        m_world.update(m_snake);
        m_fElapsedAcc -= timeStep;
        if (m_snake.hasLost())
            m_snake.reset();
    }

}

void Game::render()
{
    m_window.beginDraw();

    // Custom draw
    m_world.render(*m_window.getRenderWindow());
    m_snake.render(*m_window.getRenderWindow());
    m_textbox.render(*m_window.getRenderWindow());

    m_window.endDraw();
}
