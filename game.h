#ifndef GAME_H
#define GAME_H

#include <sstream>
#include <vector>
#include <SFML/Graphics.hpp>

#include "base_game.h"
#include "snake.h"
#include "world.h"
#include "textbox.h"

#define GAME_WINDOW_WIDTH 800
#define GAME_WINDOW_HEIGHT 600

class Game : public BaseGame
{
private:
    World m_world;
    Snake m_snake;
    Textbox m_textbox;

    float m_fElapsedAcc = 0;

public:
    Game();
    ~Game();

    void handleInput() override;
    void update() override;
    void render() override;

private:

};

#endif // GAME_H
