#ifndef SNAKESEGMENT_H
#define SNAKESEGMENT_H

#include <SFML/Graphics.hpp>
#include <vector>

#define C_SNAKE_SEGMENT_COLOR sf::Color::Yellow
#define C_SNAKE_HEAD_COLOR sf::Color::Green

struct SnakeSegment
{
    sf::Vector2i pos;
    SnakeSegment(int x, int y) : pos(x,y) {}

};

using SnakeContainer = std::vector<SnakeSegment>;

enum class Direction
{
    NONE,
    UP,
    DOWN,
    LEFT,
    RIGHT
};

#endif // SNAKESEGMENT_H
