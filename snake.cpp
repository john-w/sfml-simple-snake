#include "snake.h"

Snake::Snake(int l_blockSize, sf::Vector2i l_initPos)
{
    m_blockSize = l_blockSize;
    m_initPos = l_initPos;

    // hacks a little border
    int borderedSize = m_blockSize - 1;
    m_bodyShape.setSize(sf::Vector2f(borderedSize, borderedSize));
    reset();
}

Snake::~Snake()
{

}

int Snake::getSpeed()
{
    return m_speed;
}

int Snake::getLives()
{
    return m_lives;
}

int Snake::getScore()
{
    return m_score;
}

void Snake::increaseScore()
{
    m_score += SCORE;
}

Direction Snake::getDirection()
{
    return m_dir;
}

Direction Snake::getPhysicalDirection()
{
    if (m_snakeBody.size() <= 1) return Direction::NONE;

    SnakeSegment &head = m_snakeBody[0];
    SnakeSegment &neck = m_snakeBody[1];

    if (head.pos.x == neck.pos.x)
        return head.pos.y > neck.pos.y ? Direction::DOWN : Direction::UP;
    else if (head.pos.y == neck.pos.y)
        return head.pos.x > neck.pos.x ? Direction::RIGHT : Direction::LEFT;

    return Direction::NONE;

}

void Snake::setDirection(Direction l_dir)
{
    m_dir = l_dir;
}

sf::Vector2i Snake::getPosition()
{
    return (!m_snakeBody.empty() ? m_snakeBody.front().pos : sf::Vector2i(1,1));
}

bool Snake::hasLost()
{
    return m_lost;
}

void Snake::lose()
{
    m_lost = true;
}

void Snake::toggleLost()
{
    m_lost = !m_lost;
}

void Snake::extend()
{
    if (m_snakeBody.empty()) return;
    SnakeSegment &tailHead = m_snakeBody[m_snakeBody.size() - 1];

    if (m_snakeBody.size() > 1) {
        SnakeSegment &tailBone = m_snakeBody[m_snakeBody.size() - 2];

        int newx, newy;
        if (tailHead.pos.x == tailBone.pos.x) {
            newx = tailHead.pos.x;
            newy = (tailHead.pos.y > tailBone.pos.y) ? tailHead.pos.y+1 : tailHead.pos.y-1;
        }
        else if (tailHead.pos.y == tailBone.pos.y){
            newx = (tailHead.pos.x > tailBone.pos.x) ? tailHead.pos.x+1 : tailHead.pos.x-1;
            newy = tailHead.pos.y;
        }
        else {
            switch (m_dir) {
                case Direction::UP:
                    newx = tailHead.pos.x;
                    newy = tailHead.pos.y+1;
                    break;
                case Direction::DOWN:
                    newx = tailHead.pos.x;
                    newy = tailHead.pos.y-1;
                    break;
                case Direction::LEFT:
                    newx = tailHead.pos.x+1;
                    newy = tailHead.pos.y;
                    break;
                case Direction::RIGHT:
                    newx = tailHead.pos.x-1;
                    newy = tailHead.pos.y;
                    break;
                default:
                    return;
                    break;
            }
        }
        m_snakeBody.push_back(SnakeSegment(newx, newy));
    }
}

void Snake::reset()
{
    m_snakeBody.clear();
    m_snakeBody.push_back(SnakeSegment(m_initPos.x, m_initPos.y));
    m_snakeBody.push_back(SnakeSegment(m_initPos.x, m_initPos.y-1));
    m_snakeBody.push_back(SnakeSegment(m_initPos.x, m_initPos.y-2));

    m_lost = false;
    m_score = 0;
    setDirection(Direction::NONE);
    m_lives = INIT_LIVES;
    m_speed = INIT_SPEED;
}

void Snake::move()
{
    for (int i = m_snakeBody.size(); i > 0; --i)
        m_snakeBody[i].pos = m_snakeBody[i-1].pos;

    switch (m_dir) {
        case Direction::UP:
            --m_snakeBody[0].pos.y;
            break;
        case Direction::DOWN:
            ++m_snakeBody[0].pos.y;
            break;
        case Direction::LEFT:
            --m_snakeBody[0].pos.x;
            break;
        case Direction::RIGHT:
            ++m_snakeBody[0].pos.x;
            break;
        default:
            break;

    }
}

void Snake::tick()
{
    if (m_snakeBody.empty()) return;
    if (m_dir == Direction::NONE) return;
    move();
    checkCollision();
}

void Snake::cut(int l_segments)
{
    for (int i=0; i < l_segments; ++i)
        m_snakeBody.pop_back();

    --m_lives;
    if (!m_lives) {
        lose();
        return;
    }
}

void Snake::render(sf::RenderWindow &l_window)
{
    if (m_snakeBody.empty()) return;

    // Draw head
    auto head = m_snakeBody.begin();
    m_bodyShape.setFillColor(C_SNAKE_HEAD_COLOR);
    m_bodyShape.setPosition(static_cast<float>(m_blockSize) * static_cast<sf::Vector2f>(head->pos));
    l_window.draw(m_bodyShape);

    // Draw body
    m_bodyShape.setFillColor(C_SNAKE_SEGMENT_COLOR);
    for (auto itr = m_snakeBody.begin() + 1; itr != m_snakeBody.end(); ++itr)  // skip head
    {
        m_bodyShape.setPosition(static_cast<float>(m_blockSize) * static_cast<sf::Vector2f>(itr->pos));
        l_window.draw(m_bodyShape);
    }
}

void Snake::checkCollision()
{
    if (m_snakeBody.size() < 5) return;
    SnakeSegment &head = m_snakeBody.front();

    for (auto itr = m_snakeBody.begin() + 1; itr != m_snakeBody.end(); ++itr)
    {
        if (itr->pos == head.pos){
            int segments = m_snakeBody.end() - itr;
            cut(segments);
            break;
        }
    }
}
