#ifndef SNAKE_H
#define SNAKE_H

#include "snake-segment.h"

#define INIT_LIVES 3
#define INIT_SPEED 15
#define SCORE 10

class Snake
{
    private:
    SnakeContainer m_snakeBody;
    sf::RectangleShape m_bodyShape;
    int m_blockSize;
    sf::Vector2i m_initPos;

    Direction m_dir;
    int m_speed;

    int m_lives;
    int m_score;
    bool m_lost;

    public:
    Snake(int l_blockSize, sf::Vector2i l_initPos = sf::Vector2i(5,5));
    ~Snake();

    int getSpeed();
    int getLives();
    int getScore();
    void increaseScore();

    Direction getDirection();
    Direction getPhysicalDirection();
    void setDirection(Direction l_dir);
    sf::Vector2i getPosition();

    bool hasLost();
    void lose();
    void toggleLost();

    void extend();
    void reset();

    void move();
    void tick();
    void cut(int l_segments);

    void render(sf::RenderWindow& l_window);

    private:
    void checkCollision();
};

#endif // SNAKE_H
