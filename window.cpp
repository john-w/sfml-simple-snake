#include "window.h"

Window::Window()
{
    setup("Window", sf::Vector2u(800,600));
}

Window::Window(const std::string &l_title, const sf::Vector2u &l_size)
{
    setup(l_title, l_size);
}

Window::~Window()
{
    destroy();
}

void Window::beginDraw()
{
    m_window.clear(m_clearColor);
}

void Window::endDraw()
{
    m_window.display();
}

void Window::update()
{
    sf::Event event;
    while (m_window.pollEvent(event)) {
        if (event.type == sf::Event::Closed || (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Escape))
            m_isDone = true;
        if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::F5)
            toggleFullscreen();


    }
}

bool Window::isDone()
{
    return m_isDone;
}

bool Window::isFullscreen()
{
    return m_isFullScreen;
}

sf::Vector2u Window::getWindowSize()
{
    return m_windowSize;
}

sf::RenderWindow* Window::getRenderWindow()
{
    return &m_window;
}

void Window::toggleFullscreen()
{
    m_isFullScreen = !m_isFullScreen;
    destroy();
    create();
}

void Window::draw(sf::Drawable &l_drawable)
{
    m_window.draw(l_drawable);
}

void Window::setup(const std::string &l_title, const sf::Vector2u &l_size)
{
    m_windowTitle = l_title;
    m_windowSize = l_size;
    m_isFullScreen = false;
    m_isDone = false;
    create();
}

void Window::destroy()
{
    m_window.close();
}

void Window::create()
{
    auto style = (m_isFullScreen ? sf::Style::Fullscreen : sf::Style::Default);
    m_window.create({m_windowSize.x, m_windowSize.y, 32}, m_windowTitle, style);
    m_window.setFramerateLimit(60);
}
