#ifndef WINDOW_H
#define WINDOW_H

#include <SFML/Graphics.hpp>

class Window {
    private:
    sf::RenderWindow m_window;
    sf::Vector2u m_windowSize;
    std::string m_windowTitle;
    bool m_isDone;
    bool m_isFullScreen;
    sf::Color m_clearColor = sf::Color(12,12,90);   // Dark blue

    public:
    Window();
    Window(const std::string& l_title, const sf::Vector2u& l_size);
    ~Window();

    void beginDraw();
    void endDraw();
    void update();

    bool isDone();
    bool isFullscreen();
    sf::Vector2u getWindowSize();
    sf::RenderWindow *getRenderWindow();

    void toggleFullscreen();
    void draw(sf::Drawable& l_drawable);

    private:
    void setup(const std::string& l_title, const sf::Vector2u& l_size);
    void destroy();
    void create();
};
#endif // WINDOW_H
