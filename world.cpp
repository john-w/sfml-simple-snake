#include "world.h"

World::World(sf::Vector2i l_windowSize, int l_blockSize)
{
    m_windowSize = l_windowSize;
    m_blockSize = l_blockSize;

    respawnApple();
    m_appleShape.setFillColor(C_WORLD_APPLE);
    m_appleShape.setRadius(m_blockSize/2);

    for (int i=0; i < 4; i++)
    {
        m_bounds[i].setFillColor(C_WORLD_BOUNDARY);
        sf::Vector2f shapeSize = (!((i+1)%2))
                                 ? sf::Vector2f(m_windowSize.x, m_blockSize)
                                 : sf::Vector2f(m_blockSize, m_windowSize.y);
        m_bounds[i].setSize(shapeSize);
        if (i<2)
            m_bounds[i].setPosition(0,0);
        else {
            m_bounds[i].setOrigin(m_bounds[i].getSize());
            m_bounds[i].setPosition(sf::Vector2f(m_windowSize));
        }
    }
}

World::~World()
{

}

void World::respawnApple()
{
    // Calculate allowed boundaries
    int maxX = (m_windowSize.x / m_blockSize) - 2;
    int maxY = (m_windowSize.y / m_blockSize) - 2;
    m_item = sf::Vector2i(rand() % maxX+1, rand() % maxY+1);
    m_appleShape.setPosition(static_cast<float>(m_blockSize) * static_cast<sf::Vector2f>(m_item));
}

int World::getBlockSize()
{
    return m_blockSize;
}

void World::update(Snake &l_player)
{
    sf::Vector2i playerPos = l_player.getPosition();
    if (playerPos == m_item) {
        l_player.extend();
        l_player.increaseScore();
        respawnApple();
    }

    // Check if player bonked the walls
    sf::Vector2i grid = m_windowSize / m_blockSize;
    if (playerPos.x <= 0 || playerPos.x >= grid.x-1 || playerPos.y <= 0 || playerPos.y >= grid.y-1)
        l_player.lose();
}

void World::render(sf::RenderWindow &l_window)
{
    for (int i=0; i < 4; i++)
        l_window.draw(m_bounds[i]);

    l_window.draw(m_appleShape);

}
