#ifndef WORLD_H
#define WORLD_H

#include <SFML/Graphics.hpp>
#include "snake.h"

#define C_WORLD_BOUNDARY sf::Color(150,0,0)
#define C_WORLD_APPLE sf::Color::Red
#define DEFAULT_BLOCK_SIZE 16

class World
{
    private:
    sf::Vector2i m_windowSize;
    sf::Vector2i m_item;
    int m_blockSize;

    sf::CircleShape m_appleShape;
    sf::RectangleShape m_bounds[4];

    public:
    World(sf::Vector2i l_windowSize, int l_blockSize = DEFAULT_BLOCK_SIZE);
    ~World();

    void respawnApple();
    int getBlockSize();

    void update(Snake &l_player);
    void render(sf::RenderWindow &l_window);

};

#endif // WORLD_H
